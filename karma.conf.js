/**
 * Created by front on 2018-01-23.
 */

var webpackConfig = require('./build/webpack.dev.conf.js')
delete webpackConfig.entry

// karma.conf.js
module.exports = function (config) {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['jasmine'],
    // 이 파일은 모든 테스트의 시작점입니다.
    files: ['test/index.js'],
    // 우리는 번들을 위해 webpack에 엔트리 파일을 전달할 것입니다.
    preprocessors: {
      'test/index.js': ['webpack']
    },
    // webpack 설정을 사용합니다.
    webpack: webpackConfig,
    // 쓸모없는 텍스트들을 피합니다.
    webpackMiddleware: {
      noInfo: true
    },
    autoRun: true,
    singleRun: false
  })
}
