// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueI18n from 'vue-i18n'
import 'expose-loader?$!expose-loader?jQuery!jquery'
import axios from 'axios'
import 'materialize-css/dist/js/materialize.min.js'
import generalHelpers from './components/helper';
import Meta from 'vue-meta'

Vue.use(generalHelpers);
Vue.use(VueI18n);
Vue.use(Meta);
// i18n
import enTranslation from './i18n/strings-en.json' // English
import koTranslation from './i18n/strings-ko.json' // korean

// Ready translated locale messages
const messages = {
  en: {
    message: enTranslation
  },
  ko: {
    message: koTranslation
  }
};

let lang = window.localStorage.getItem('lang') || 'en';
// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: lang, // set locale
  messages // set locale messages
});

Vue.config.productionTip = false;
Vue.prototype.$http = axios.create({
  baseURL: '/',
  responseType: 'json'
});  // ajax request lib 전역 선언

// Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Sec bGhpMjM1cjk4YWVyZ3Q5MDhlcmlpZWxkZmpiOWZ4OGdldFMkRVl0dXM5ZXI=';
Vue.prototype.$http.defaults.headers.post['Content-Type'] = 'multipart/form-data';
Vue.prototype.$http.defaults.headers.post['X-Requested-With'] = 'XMLHttpRequest';
// Vue.prototype.$http.defaults.headers.put['Content-Type'] = 'multipart/form-data';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  mounted: function () {
  },
  template: '<App/>',
  components: { App }
});
