export default {
  name: 'util-typer',
  props: {
    content: {
      type: String,
      twoWay: true
    },
    preTypeDelay: {
      type: Number,
      default: 100
    },
    typeDelay: {
      type: Number,
      default: 150
    },
    infinite: {
      type: Boolean,
      default: true
    }
  },
  data: function () {
    return {
      actionTimeout: 0,
      actionInterval: 0,
      currentText: '',
      currentIndex: -1,
      isDoneTyping: false
    }
  },
  watch: {
    content: function (newVal, oldVal) {
      this.init();
    }
  },
  computed: {
    splitedArray: function () {
      return this.content.split('');
    }
  },
  mounted () {
    this.$nextTick(function () {
      this.init();
    });
  },
  methods: {
    init () {
      this.actionTimeout = 0;
      this.actionInterval = 0;
      this.currentText = '';
      this.currentIndex = -1;
      this.isDoneTyping = false;
      this.currentText = '';
      this.reverse = false;
      this.startTyping();
    },
    startTyping () {
      this.actionTimeout = setTimeout(() => {
        this.typeStep();
        if (!this.isDoneTyping) {
          this.actionInterval = setInterval(this.typeStep, this.typeDelay)
        }
      }, this.preTypeDelay)
    },
    typeStep () {
      if (this.reverse === false) {
        this.forwardTypeStep();
      }

      if (!this.infinite && this.currentIndex === this.splitedArray.length - 1) {
        this.isDoneTyping = true;
        this.cancelCurrentAction();
      } else if (this.infinite === true && this.currentIndex === this.splitedArray.length - 1) {
        this.reverse = true;
        this.reverseTypeStep();
      }
    },
    forwardTypeStep () {
      this.currentText += this.splitedArray[++this.currentIndex];
    },
    reverseTypeStep () {
      this.currentText = this.currentText.slice(0, -1);
      if (this.currentText.length === 0) {
        this.reverse = false;
        this.currentIndex = -1;
      }
    },
    cancelCurrentAction () {
      if (this.actionInterval) {
        clearInterval(this.actionInterval)
        this.actionInterval = 0
      }
      if (this.actionTimeout) {
        clearTimeout(this.actionTimeout);
        this.actionTimeout = 0
      }
    }
  },
  components: {
  }

}
