﻿import ScrollMagic from 'scrollmagic';

/*
 * Depengency:
 * jQuery, TweenMax
 * ScrollMagic : npm run --save scrollmagic
 *
 * @param  { Array }
 const historyScenes = [
 {
 element: '#Year-2010',
 classToggle: [{ element: '#Page-2 path', className: 'active-1' }]
 }]

 const tl4 = new TimelineMax();
 tl1.add(TweenMax.from('#aboutus-one', 0.5, {yPercent: 25, ease: Power1.easeOut}));
 const scenes = [
 {
 option: { triggerHook: 0.5, offset: -150 },
 element: '#aboutus-one',
 tween: tl1
 }]
 * */
// TODO 반복된 코드가 있다. 맘에 안든다 추후 리팩토링 필요.
const ScrollMagicFactory = (() => {
  const ScrollMagicFactory = function ScrollMagicFactory () {
    this.controller = null;
    this.scenesWithTween = [];
    this.scenesAutoVideo = [];
    this.scenesClassToggle = [];
  };

  let _scene;

  ScrollMagicFactory.prototype.init = function () {
    this.controller = new ScrollMagic.Controller();
  };
  ScrollMagicFactory.prototype.createMultipleSceneWithTween = function (scenes) {
    let commonOption = {};
    let indicator = false;
    if (arguments.length === 2) {
      if (typeof arguments[1] === 'object') {
        commonOption = arguments[1];
      } else if (typeof arguments[1] === 'boolean') {
        indicator = arguments[1];
      }
    }
    if (arguments.length === 3) {
      if (typeof arguments[1] === 'object') {
        commonOption = arguments[1];
        indicator = arguments[2];
      } else {
        return new Error('no such type of parameter');
      }
    }
    if (typeof scenes !== 'object') return new Error('no element parameter!!!');

    if (Array.isArray(scenes)) {
      scenes.forEach((scene) => {
        _scene = this.createSingleScene(scene, commonOption, indicator);
        _scene.addTo(this.controller);
        this.scenesWithTween.push(_scene);
      });
    } else {
      _scene = this.createSingleScene(scenes, commonOption, indicator);
      _scene.addTo(this.controller);
      this.scenesWithTween.push(_scene);
    }
    return this;
  };
  ScrollMagicFactory.prototype.createMultipleSceneClassToggle = function (scenes) {
    let commonOption = {};
    let indicator = false;
    if (arguments.length === 2) {
      if (typeof arguments[1] === 'object') {
        commonOption = arguments[1];
      } else if (typeof arguments[1] === 'boolean') {
        indicator = arguments[1];
      }
    }
    if (arguments.length === 3) {
      if (typeof arguments[1] === 'object') {
        commonOption = arguments[1];
        indicator = arguments[2];
      } else {
        return new Error('no such type of parameter');
      }
    }
    if (typeof scenes !== 'object') return new Error('no element parameter!!!');
    if (Array.isArray(scenes)) {
      scenes.forEach((scene) => {
        _scene = this.createSingleScene(scene, commonOption, indicator);
        _scene.addTo(this.controller);
        this.scenesClassToggle.push(_scene);
      });
    } else {
      _scene = this.createSingleScene(scenes, commonOption, indicator);
      _scene.addTo(this.controller);
      this.scenesClassToggle.push(_scene);
    }
    return this;
  };
  ScrollMagicFactory.prototype.createSingleScene = function (scene, common, indicator) {
    if (typeof scene !== 'object') return new Error('no element parameter!!!');
    _scene = new ScrollMagic.Scene(Object.assign({}, { triggerElement: scene.element, reverse: false }, scene.option, common));
    if (scene.tween !== undefined) {
      _scene.setTween(scene.tween);
    }
    if (scene.classToggle !== undefined && scene.classToggle.length > 0) {
      scene.classToggle.forEach(function (item) {
        _scene.setClassToggle(item.element, item.className);
      });
    }
    if (indicator) {
      _scene.addIndicators(indicator);
    }

    return _scene;
  };
  ScrollMagicFactory.prototype.createScrollAutoVideo = function (videoScenes) {
    let commonOption = {};
    let indicator = false;
    if (arguments.length === 2) {
      if (typeof arguments[1] === 'object') {
        commonOption = arguments[1];
      } else if (typeof arguments[1] === 'boolean') {
        indicator = arguments[1];
      }
    }
    if (arguments.length === 3) {
      if (typeof arguments[1] === 'object') {
        commonOption = arguments[1];
        indicator = arguments[2];
      } else {
        return new Error('no such type of parameter');
      }
    }
    if (typeof videoScenes !== 'object') return new Error('no element parameter!!!');

    if (Array.isArray(videoScenes)) {
      videoScenes.forEach((scene) => {
        _scene = this.createSingleScene(scene, commonOption, indicator);
        _scene.on('enter', () => {
          if (scene.player.paused) {
            scene.player.play();
          }
        });
        _scene.on('leave', () => {
          if (scene.player.played.length > 0) {
            scene.player.pause();
          }
        });
        _scene.addTo(this.controller);
        this.scenesAutoVideo.push(_scene);
      });
    } else {
      _scene = this.createSingleScene(videoScenes, commonOption, indicator);
      _scene.on('enter', () => {
        videoScenes.player.play();
      })
      _scene.on('leave', () => {
        videoScenes.player.pause();
      });
      _scene.addTo(this.controller);
      this.scenesAutoVideo.push(_scene);
    }
  }
  ScrollMagicFactory.prototype.removeAllScene = function () {
    this.controller.destroy(true);
    this.controller = null;
    this.scenesWithTween.every((scene) => {
      scene.destroy(true);
      return true;
    });
    this.scenesAutoVideo.every((scene) => {
      scene.destroy(true);
      return true;
    });
    this.scenesClassToggle.every((scene) => {
      scene.destroy(true);
      return true;
    });
    this.scenesWithTween = [];
    this.scenesAutoVideo = [];
    this.scenesClassToggle = [];
    console.log('destroyed', this);
  };

  ScrollMagicFactory.prototype.updateAllScene = function () {
    this.scenesWithTween.every((scene) => {
      scene.refresh();
      return true;
    });
    this.scenesAutoVideo.every((scene) => {
      scene.refresh();
      return true;
    });
    this.scenesClassToggle.every((scene) => {
      scene.refresh();
      return true;
    });
  }
  ScrollMagicFactory.prototype.updateAllVideoDuration = function (duration) {
    this.scenesAutoVideo.every((scene) => {
      scene.duration(duration);
      return true;
    });
  }
  return new ScrollMagicFactory();
})();

export default ScrollMagicFactory
