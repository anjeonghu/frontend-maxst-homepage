/**
 * Created by front on 2018-01-24.
 */
const errorMessages = {
  fieldIsRequired: '필드를 입력하세요.',
  invalidFile: '{0} 파일만 업로드 가능합니다. 최대 용량 100MB'
};
const msg = (text, ...args) => { // arrow 함수에서는 arguments가 사라지고 args 가 생겻다.
  if (text != null && args.length > 0) {
    for (let i = 0; i < args.length; i++) {
      text = text.replace('{' + i + '}', args[i]);
    }
  }
  return text;
};

const checkEmpty = (filed, messges = errorMessages) => {
  if (filed.value === null || filed.value === '') {
    if (filed.required) {
      return {
        valid: false,
        error: msg(messges.fieldIsRequired)
      }
    }
  }
  return {
    valid: true,
    error: ''
  }
};

const checkFile = (file, mode = 'image', messages = errorMessages) => {
  const type = file.type;
  const size = file.size;
  if (file != null) {
    if (mode === 'image') {
      if (!/(image\/gif)|(image\/png)|(image\/jpeg)/gi.test(type) || size > (100 * 1024 * 1024)) {
        return {
          valid: false,
          error: msg(messages.invalidFile, '(gif, jpg, png, jpeg)')
        }
      }
    } else if (mode === 'zip') {
      if (!/(application\/zip)|(application\/octet-stream)|(application\/x-zip-compressed)/gi.test(type) || size > (100 * 1024 * 1024)) {
        return {
          valid: false,
          error: msg(messages.invalidFile, '(zip)')
        }
      }
    } else if (mode === 'map') {
      if (size > (100 * 1024 * 1024)) {
        return {
          valid: false,
          error: msg(messages.invalidFile, '(map)')
        }
      }
    }
  }

  return {
    valid: true,
    error: ''
  };
};

const checkField = (filed) => {
  let validation;
  let validatorFunc = filed.validator || validator.required;
  validation = validatorFunc(filed);
  return validation;
};

const checkAllField = (scheme) => {
  let err = {};
  let obj = {};
  if (scheme != null) {
    Object.keys(scheme).forEach((key) => {
      let field = scheme[key];
      let validation = checkField(field);
      err[key] = validation;
      if (!err[key].valid) {
        Object.assign(obj, err)
      }
    });
    return obj;
  }
  return {};
};

const validator = {
  required (filed, messges = errorMessages) {
    const result = checkEmpty(filed, messges = errorMessages)
    return result;
  },
  one: checkField,
  file: checkFile,
  all: checkAllField
};

export default validator;
