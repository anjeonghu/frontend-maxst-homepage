import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/home/Home.vue'
import ARSDK from '../pages/arsdk/ARSDK.vue'
import ARCollaboration from '../pages/arcollaboration/ARCollaboration.vue'
import ARGuide from '../pages/arguide/ARGuide.vue'
import CaseStudies from '../pages/casestudies/CaseStudies.vue'
import VirtualGuide from '../pages/casestudies/detail/VirtualGuide.vue'
import RmoteSolution from '../pages/casestudies/detail/RmoteSolution.vue'
import SmartFactory from '../pages/casestudies/detail/SmartFactory.vue'
import KIAManual from '../pages/casestudies/detail/KIAManual.vue'
import AboutUs from '../pages/aboutus/AboutUs.vue'
import ContactUs from '../pages/contactus/ContactUs.vue'

Vue.use(Router);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: 'Home Page - Example App',
      metaTags: [
        {
          name: 'description',
          content: 'The home page of our example app.'
        },
        {
          property: 'og:description',
          content: 'The home page of our example app.'
        }
      ]
    }
  },
  {
    path: '/en/',
    name: 'en.home',
    component: Home
  },
  {
    path: '/ko/arsdk',
    name: 'arsdk',
    component: ARSDK
  },
  {
    path: '/en/arsdk',
    name: 'en.arsdk',
    component: ARSDK
  },
  {
    path: '/ko/arcollaboration',
    name: 'arcollaboration',
    component: ARCollaboration
  },
  {
    path: '/en/arcollaboration',
    name: 'en.arcollaboration',
    component: ARCollaboration
  },
  {
    path: '/ko/arguide',
    name: 'arguide',
    component: ARGuide
  },
  {
    path: '/en/arguide',
    name: 'en.arguide',
    component: ARGuide
  },
  {
    path: '/ko/casestudies',
    name: 'casestudies',
    component: CaseStudies
  },
  {
    path: '/en/casestudies',
    name: 'en.casestudies',
    component: CaseStudies
  },
  {
    path: '/ko/casestudies/virtualguide',
    name: 'casestudies.virtualguide',
    component: VirtualGuide
  },
  {
    path: '/en/casestudies/virtualguide',
    name: 'en.casestudies.virtualguide',
    component: VirtualGuide
  },
  {
    path: '/ko/casestudies/remotesolution',
    name: 'casestudies.remotesolution',
    component: RmoteSolution
  },
  {
    path: '/en/casestudies/remotesolution',
    name: 'en.casestudies.remotesolution',
    component: RmoteSolution
  },
  {
    path: '/ko/casestudies/kiamanual',
    name: 'casestudies.kiamanual',
    component: KIAManual
  },
  {
    path: '/en/casestudies/kiamanual',
    name: 'en.casestudies.kiamanual',
    component: KIAManual
  },
  {
    path: '/en/casestudies/smartfactory',
    name: 'en.casestudies.smartfactory',
    component: SmartFactory
  },
  {
    path: '/ko/casestudies/smartfactory',
    name: 'casestudies.smartfactory',
    component: SmartFactory
  },
  {
    path: '/ko/aboutus',
    name: 'aboutus',
    component: AboutUs
  },
  {
    path: '/en/aboutus',
    name: 'en.aboutus',
    component: AboutUs
  },
  {
    path: '/ko/contactus',
    name: 'contactus',
    component: ContactUs
  },
  {
    path: '/en/contactus',
    name: 'en.contactus',
    component: ContactUs
  },
  {
    path: '/*',
    redirect: '/'
  }
];

export default new Router({
//  mode: 'history',
  base: __dirname,
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
