# frontend-vue-project

[사이트 링크](http://maxst.com/)
![image](https://github.com/happenask/staticStorage/blob/master/images/maxst%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80.PNG?raw=true)


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
